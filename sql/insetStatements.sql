INSERT INTO branch (branchName, address, tel)
VALUES ("Ealing2", "12 Ealing broadway2, London, W13 2OH2", "021111111112"),
("Greenwitch", "Meridian House, Royal Hill, London SE10 8RD", "02085167800");

INSERT INTO role (role, authorization)
VALUES ("Manager", "Admin"),
("Sales Person", "Staff"),
("Administrator", "Admin");

INSERT INTO brand (`name`)
VALUES ("Toyota"),
("BMW"),
("Audi"),
("Acura");

INSERT INTO equipment (feature, required)
VALUES ("Engine", "Y"),
("Color", "Y"),
("Seat", "Y"),
("Wheel", "Y"),
("Option", "N");

INSERT INTO employee (firstName, lastName, address,
tel, username, `password`, branchId, startDate, roleId)
VALUES ("Shino", "Hartmann", "5 Street, London, SE10 10PP",
 "07777777777", "shino123", "password", 2, "2014-05-01", 2);

INSERT INTO customer (firstName, lastName, address, tel, email, branchId, salesPerson)
VALUES ("Airi", "Satou", "5 Manor Cl, Cossington, Bridgwater, Somerset TA7 8JT, UK", "07700900767", "AiriSatou@gmail.com", 2, 1),
("Angelica", "Ramos", "6 Porchester Terrace, London W2 3TL, UK","07700900212","AngelicaRamos@gmail.com",2,1);

INSERT INTO `order` (customerId)
VALUES (1);

/* 05/11 added */
INSERT INTO employee (firstName, lastName, address,
tel, username, `password`, branchId, startDate, roleId)
VALUES ("Aleksandra", "Aftewicz", "6 Street, London, SE11 10PP, UK",
"07700900266",  "alex123", "password",  2, "2014-06-01", 1),
 ("Ted", "Mosby", "37A New Chester Rd, Birkenhead, Wirral, Merseyside CH62 1AA, UK",
"07700900213",  "ted123", "password",  2, "2014-06-01", 2),
  ("Robin", "Scherbatsky", "55 Buxton Ave, Heanor, Derbyshire DE75 7UN, UK",
"07700900966",  "ashton123", "password",  2, "2014-06-01", 3);

INSERT INTO model (`name`, brandId, price)
VALUES ("Prius", 1, 21995.00), ("GT86", 1, 22495.00),("AYGO", 1, 7995.00),
("M3 Saloon", 2, 56595.00), ("X1", 2, 26780.00), ("X3", 2, 33795.00),
("A6 Avant", 3, 36185.00), ("R8 Coupe", 3, 95000.00), ("S3", 3, 30230.00),
("New Fiat 500", 4, 10890.00), ("Fiat PANDA", 4, 9375.00), ("Fiat PUNTO", 4, 10990.00);

/*
INSERT INTO equipment (feature, required)
VALUES ("Rear spoiler", "N"),
("Sound", "N"),
("Child seat", "N"),
("Rear parking sensor", "N"),
("Rear bicycle holder",  "N"),
("trim", "N"),
("Ash tray", "N");
*/

INSERT INTO modelEquipment (modelId, equipmentId, details, price)
VALUES
(1, 1, "1.8 Hybrid Automatic Multidrive S", 0),
(1, 2, "Pure White", 0),
(1, 2, "Astral Black", 0),
(1, 2, "Tyrol Silver", 495.00),
(1, 2, "Vermillion Red", 495.00),
(1, 2, "Dark Blue", 495.00),
(1, 2, "White Pearl", 650.00),
(1, 2, "Novus Grey", 495.00),
(1, 4, '15" alloy wheels (5-spoke)', 0),
(1, 3, "Awara Black Cloth", 0),
(1, 3, "Leather Seats", 1500),
(1, 5, "Toyota Touch & Go Navigation", 750.00),
(1, 5, "Baby-Safe Plus belted base", 95.00),
(1, 5, "Kidfix-ISOFIX child seat", 175.00),
(1, 5, "Rear parking sensor kit", 295.00),
(2, 1, "2.0L petrol Boxer (200 HP) 6 Speed Manual", 0),
(2, 1, "2.0L petrol Boxer (200 HP) 6 Speed Automatic", 995.00),
(2, 2, "GT Pure Red", 0),
(2, 2, "GT Starlight Blue", 495.00),
(2, 2, "GT White Pearl", 650.00),
(2, 2, "GT Black", 495.00),
(2, 2, "GT Orange", 495.00),
(2, 4, '17" Alloy wheels (18" Standard on Blanco)', 0),
(2, 4, '18" 10 spoke Alloy', 1395.00),
(2, 5, "Rear spoiler", 0),
(2, 3, "Black Fabric", 0),
(2, 5, "JBL Sound Pack", 1100.00),
(2, 5, "Toyota Touch & Go Navigation", 750.00),
(2, 5, "Baby-Safe Plus belted base", 95.00),
(2, 5, "Kidfix-ISOFIX child seat", 175.00),
(3, 1, "1.0 L 5 Speed Manual", 0),
(3, 2, "Red Pop", 0),
(3, 2, "White Flash", 250.00),
(3, 2, "Electro Grey", 495.00),
(3, 2, "Silver Splash", 495.00),
(3, 4, '14" steel wheels with wheel caps (9-spoke)', 0),
(3, 4, '15" black alloy wheels (5 double-spoke)', 700.00),
(3, 4, '15" black alloy wheels (5-spoke)', 700.00),
(3, 4, '15" black machined-face alloy wheels (10-spoke)', 700.00),
(3, 4, '15" black machined-face alloy wheels (5 double-spoke)', 700.00),
(3, 4, '15" Winter rim', 331.50),
(3, 3, "Mod fabric", 0),
(3, 5, "AM/FM radio", 50.00),
(3, 5, "Baby-Safe Plus belted base", 95.00),
(3, 5, "Kidfix-ISOFIX child seat", 175.00),
(3, 5, "Rear bicycle holder kit", 745.00),
(3, 5, "Anthracite carpet mats with anthracite trim", 50.00),
(3, 5, "Anthracite carpet mats with dark grey trim", 50.00),
(3, 5, "Anthracite carpet mats with silver trim", 50.00),
(3, 5, "Anthracite carpet mats with red trim", 50.00),
(3, 5, "Ash tray", 40.00),
(4, 1, "Manual", 0),
(4, 1, "Seven-speed Double Clutch Transmission (M DCT) w/ DRIVELOGIC", 2495.00),
(4, 2, "Mineral White", 0),
(4, 2, "Alpine WhiteMineral Gray", 0),
(4, 2, "Austin Yellow Metallic", 0),
(4, 2, "Azurite Black", 950.00),
(4, 2, "Champagne Quartz", 950.00),
(4, 2, "Tanzanite Blue", 950.00),
(4, 2, "Smoked Topaz", 950.00),
(4, 4, '19" M Double- spoke style 437M alloy wheels with mixed tyres', 0),
(4, 4, '19" M Double- spoke style 437M alloy wheels/Black with mixed', 175.00),
(4, 3, "Silverstone Extended Merino Leather", 0),
(4, 3, "Sakhir Orange Extended Merino Leather", 0),
(4, 3, "Silverstone Full Merino Leather", 1330.00),
(4, 3, "Sakhir Orange Full Merino Leather", 1330.00),
(4, 3, "Golden Brown Merino lthr, BMW Individual", 255.00),
(4, 3, "Nutmeg Merino lthr - BMW Individual", 255.00),
(4, 3, "Opal White Merino lthr - BMW Individua", 255.00),
(4, 3, "Amaro Brown Merino lthr - BMW Individual", 255.00),
(4, 3, "Cohiba Brown Merino lthr - BMW Individual", 255.00),
(4, 5, "Auburn Sycamore wood trim, high-gloss BMW Ind.", 375.00),
(4, 5, "Piano Black - BMW Ind.", 375.00),
(4, 5, "Ash Grain White - BMW Individual interior trim", 375.00),
(4, 5, "Fineline Anthracite wood interior trim w/ Pearl Chrome finis", 300.00),
(4, 5, "Blue Shadow interior trim w/ High-gloss Black finishers", 0),
(4, 5, "Aluminium Blade interior trim w/ Black Chrome finisher", 300.00),
(4, 5, "Navigation system Professional", 0),
(4, 5, "Smokers package", 25.00),
(4, 5, "Seat heating", 280.00);

INSERT INTO orderequipment (orderId, modelEquipmentId)
VALUE (1, 1),(1, 3),(1, 9),(1, 10),(1, 12), (1, 13);

UPDATE `order`
SET vin="1A9XAB5S229X25669", registrationNo="wx3 wk12"
WHERE id = 1;