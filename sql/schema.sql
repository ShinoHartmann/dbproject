
    CREATE DATABASE 'cardealer';

    CREATE USER 'gsm'@'%' IDENTIFIED BY 'password';
    GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP ON cardealer.* TO 'gsm'@'%';
    FLUSH PRIVILEGES;

    use cardealer;

    CREATE TABLE role (
      id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
      role varchar(20) NOT NULL UNIQUE,
      authorization varchar(20) DEFAULT 'Staff' CHECK (authorization IN ('Admin', 'Staff')),
      PRIMARY KEY (id),
      UNIQUE KEY id_role_unique (role, authorization);
    );

    CREATE TABLE branch (
        id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
        address varchar(100) NOT NULL,
        tel varchar(25) NOT NULL,
        PRIMARY KEY (id)
    );

    CREATE TABLE employee (
      id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
      firstName varchar(40) NOT NULL,
      lastName varchar(40) NOT NULL,
      address varchar(250) NOT NULL,
      tel varchar(25) NOT NULL,
      username varchar(40) NOT NULL,
      password varchar(40) NOT NULL,
      branchId int(11) NOT NULL,
      startDate Date,
      roleId int(11) NOT NULL,
      PRIMARY KEY (id),
      FOREIGN KEY (branchId) REFERENCES branch(id),
      FOREIGN KEY (roleId) REFERENCES role(id),
      UNIQUE KEY unique_employee (firstName, lastName, tel)
    );

    CREATE TABLE brand (
        id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
        name varchar(100) NOT NULL UNIQUE,
        PRIMARY KEY (id)
    );

    CREATE TABLE model (
        id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
        name varchar(100) NOT NULL UNIQUE,
        brandId int(11) NOT NULL,
        price numeric(15,2) NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (brandId) REFERENCES brand(id)
    );

    CREATE TABLE equipment (
        id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
        feature varchar(100) NOT NULL,
        required enum('Y', 'N') NOT NULL DEFAULT 'Y',
        PRIMARY KEY (id),
    );

    CREATE TABLE modelEquipment (
        id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
        modelId int(11) NOT NULL,
        equipmentId int(11) NOT NULL,
        details varchar(255) NOT NULL,
        price numeric(15,2) NOT NULL,
        availability enum('Y', 'N') NOT NULL DEFAULT 'Y',
        memo text DEFAULT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (modelId) REFERENCES model(id),
        FOREIGN KEY (equipmentId) REFERENCES equipment(id),
        UNIQUE KEY model_equipment_details (modelId, equipmentId, details)
    );

    CREATE TABLE customer (
      id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
      firstName varchar(40) NOT NULL,
      lastName varchar(40) NOT NULL,
      address varchar(250),
      tel varchar(25) NOT NULL,
      email varchar(100),
      branchId int(11) NOT NULL,
      PRIMARY KEY (id),
      FOREIGN KEY (branchId) REFERENCES branch(id),
      UNIQUE KEY unique_customer (firstName, lastName, tel)
    );

    CREATE TABLE `order` (
        id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
        customerId int(11) NOT NULL,
        vin varchar(17) UNIQUE,
        registrationNo char(8) UNIQUE,
        orderDate date default NULL,
        salesPerson int(11) NULL,
        totalPrice numeric(15,2) NULL,
        memo text DEFAULT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (customerId) REFERENCES customer(id),
        FOREIGN KEY (salesPerson) REFERENCES employee(id);
    );

    CREATE TABLE orderEquipment (
        id int(11) NOT NULL AUTO_INCREMENT UNIQUE,
        orderId int(11) NOT NULL,
        modelEquipmentId int(11) NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (orderId) REFERENCES `order`(id),
        FOREIGN KEY (modelEquipmentId) REFERENCES modelEquipment(id),
        UNIQUE KEY unique_order_equipment (orderId, modelEquipmentId)
    );

    ALTER TABLE `order`
    ADD COLUMN salesPerson int(11) NULL AFTER `orderDate`,
    ADD FOREIGN KEY (salesPerson) REFERENCES employee(id);

    ALTER TABLE `order`
    ADD COLUMN totalPrice numeric(15,2) NULL AFTER salesPerson;

    -- 17.11 added TRIGGER

    DELIMITER $$
    CREATE DEFINER=`gsm`@`localhost` TRIGGER storeTotalPrice AFTER INSERT
    ON orderequipment
    FOR EACH ROW
    BEGIN
        UPDATE `order` o
        SET o.totalPrice =
            (SELECT
                (SUM(modelEquipment.price) + model.price) FROM model
                INNER JOIN modelEquipment on model.id=modelEquipment.modelId
                INNER JOIN orderEquipment on orderEquipment.modelEquipmentId = modelEquipment.id
                WHERE orderEquipment.orderId = o.id
            )
        WHERE o.id = NEW.orderId;
    END

    DELIMITER $$
    CREATE DEFINER=`gsm`@`localhost` TRIGGER `checkOrderId` BEFORE INSERT
    ON `orderequipment`
    FOR EACH ROW
    BEGIN
        DECLARE existingModelId INT;
        DECLARE modelIdToInsert INT;
        SET existingModelId = (
            SELECT DISTINCT m.modelId from modelequipment m
            INNER JOIN orderEquipment o on m.id = o.modelEquipmentId
            where o.orderId = NEW.orderId
            );
        SET modelIdToInsert = (
            SELECT modelId from modelequipment
            where id = NEW.modelEquipmentId
            );
        if existingModelId IS NOT NULL AND existingModelId <> modelIdToInsert then
            set @message_text = concat('New model id >', modelIdToInsert, '< does not match with existing one: >', existingModelId, '<');
            SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message_text;
        END if;
    END

    DELIMITER $$
    CREATE DEFINER=`gsm`@`localhost` TRIGGER `checkVINlength` BEFORE UPDATE ON `order`
    FOR EACH ROW
    BEGIN
        IF NEW.vin <> OLD.vin OR NEW.vin IS NOT NULL THEN
            IF CHAR_LENGTH(NEW.vin) <> 17 THEN
                set @message_text = concat('Length of vin should be 17 length: ', NEW.vin);
                SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message_text;
            END IF;
        END IF;
    END

/* 30.11. Updated */
-- orderEquipment table
USE `cardealer`;
DELIMITER $$
CREATE DEFINER=`gsm`@`localhost` TRIGGER StoreTotalPriceWhenInsert AFTER INSERT
ON orderequipment
FOR EACH ROW
BEGIN
	UPDATE `order` o
	SET o.totalPrice =
		(SELECT
			(SUM(modelEquipment.price) + model.price) FROM model
			INNER JOIN modelEquipment ON model.id=modelEquipment.modelId
			INNER JOIN orderEquipment ON orderEquipment.modelEquipmentId = modelEquipment.id
			WHERE orderEquipment.orderId = o.id
		)
	WHERE o.id = NEW.orderId;
END

USE `cardealer`;
DELIMITER $$
CREATE DEFINER=`gsm`@`localhost` TRIGGER StoreTotalPriceWhenUpdate AFTER UPDATE
ON orderequipment
FOR EACH ROW
BEGIN
	UPDATE `order` o
	SET o.totalPrice =
		(SELECT
			(SUM(modelEquipment.price) + model.price) FROM model
			INNER JOIN modelEquipment ON model.id=modelEquipment.modelId
			INNER JOIN orderEquipment ON orderEquipment.modelEquipmentId = modelEquipment.id
			WHERE orderEquipment.orderId = o.id
		)
	WHERE o.id = NEW.orderId;
END

USE `cardealer`;
DELIMITER $$
CREATE DEFINER=`gsm`@`localhost` TRIGGER CheckModelIdWhenInsert BEFORE INSERT
ON orderequipment
FOR EACH ROW
BEGIN
	DECLARE existingModelId INT;
	DECLARE modelIdToInsert INT;
	SET existingModelId = (
		SELECT DISTINCT m.modelId FROM modelequipment m
		INNER JOIN orderEquipment o ON m.id = o.modelEquipmentId
		WHERE o.orderId = NEW.orderId
		);
	SET modelIdToInsert = (
		SELECT modelId from modelequipment
		where id = NEW.modelEquipmentId
		);
	IF existingModelId IS NOT NULL AND existingModelId <> modelIdToInsert THEN
		SET @message_text = concat('New model id <', modelIdToInsert, '> does not match with existing one: <', existingModelId, '>');
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message_text;
	END IF;
END

USE `cardealer`;
DELIMITER $$
CREATE DEFINER=`gsm`@`localhost` TRIGGER CheckModelIdWhenUpdate BEFORE UPDATE
ON orderequipment
FOR EACH ROW
BEGIN
	DECLARE existingModelId INT;
	DECLARE modelIdToInsert INT;
	SET existingModelId = (
		SELECT DISTINCT m.modelId FROM modelequipment m
		INNER JOIN orderEquipment o ON m.id = o.modelEquipmentId
		WHERE o.orderId = NEW.orderId
		);
	SET modelIdToInsert = (
		SELECT modelId from modelequipment
		where id = NEW.modelEquipmentId
		);
	IF existingModelId IS NOT NULL AND existingModelId <> modelIdToInsert THEN
		SET @message_text = concat('New model id <', modelIdToInsert, '> does not match with existing one: <', existingModelId, '>');
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message_text;
	END IF;
END

-- order table
USE `cardealer`;
DELIMITER $$
CREATE DEFINER=`gsm`@`localhost` TRIGGER CheckVINlengthWhenInsert BEFORE INSERT ON `order`
FOR EACH ROW
BEGIN
	IF CHAR_LENGTH(NEW.vin) <> 17 THEN
		set @message_text = concat('Length of vin should be 17 characters: ', NEW.vin);
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message_text;
	END IF;
END

USE `cardealer`;
DELIMITER $$
CREATE DEFINER=`gsm`@`localhost` TRIGGER CheckVINlengthWhenUpdate BEFORE UPDATE ON `order`
FOR EACH ROW
BEGIN
	IF NEW.vin <> OLD.vin OR NEW.vin IS NOT NULL THEN
		IF CHAR_LENGTH(NEW.vin) <> 17 THEN
			set @message_text = concat('Length of vin should be 17 characters: ', NEW.vin);
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message_text;
		END IF;
	END IF;
END

-- Employee table
USE `cardealer`;
DELIMITER $$
CREATE DEFINER=`gsm`@`%` TRIGGER PasswordHashWhenInsert BEFORE INSERT ON employee
FOR EACH ROW
BEGIN
	DECLARE newPassword varchar(50);
	SET newPassword = NEW.`password`;
	IF char_length(newPassword) < 8 THEN
		SET @message_text = concat('Please choose a password with more than 8 characters');
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message_text;
	ELSE
		SET NEW.`password` = password(newPassword);
	END IF;
END

USE `cardealer`;
DELIMITER $$
CREATE DEFINER=`gsm`@`%` TRIGGER PasswordHashWhenUpdate BEFORE UPDATE ON employee
FOR EACH ROW
BEGIN
	DECLARE newPassword varchar(50);
	IF OLD.`password` <> NEW.`password` then
		SET newPassword = NEW.`password`;
		IF char_length(newPassword) < 8 THEN
			SET @message_text = concat('Please choose a password with more than 8 characters');
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = @message_text;
		ELSE
			SET NEW.`password` = password(newPassword);
		END IF;
	END IF;
END

-- For audit log
    SET global log_output = 'FILE';
    SET global general_log_file='c:/Users/blume/Desktop/GSM/DbManagement/mysql_general.log';
    SET global general_log = 1;

    -- add privilege to gsm staff
    GRANT INSERT, UPDATE ON `cardealer`.`customer` TO `gsmStaff`@`localhost` IDENTIFIED BY 'password';
    GRANT INSERT, UPDATE ON `cardealer`.`order` TO `gsmStaff`@`localhost` IDENTIFIED BY 'password';
    GRANT INSERT, UPDATE ON `cardealer`.`orderequipment` TO `gsmStaff`@`localhost` IDENTIFIED BY 'password';

