<?php
function openFirstDbConnection() {
    $servername = "localhost:3306";
    $username = "gsmAdmin";
    $password = "password";
    $dbname = "cardealer";
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
function openDbConnection() {
    session_start();
    if (!isset($_SESSION["AUTHORIZATION"])) {
      header("Location: logout.php");
      exit;
    }
    $servername = "localhost:3306";
    $username = "gsmAdmin";
    $password = "password";
    $dbname = "cardealer";
    if($_SESSION["AUTHORIZATION"] == "Staff") {
        $username = "gsmSales";
        $password = "password";
    }
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}
?>