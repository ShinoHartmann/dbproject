<?php
    include '../includes/db.php';
    $conn = openDbConnection();

    $array = array();
    $brandId = $_GET['brandId'];
    $sql = "SELECT id, CONCAT(name, ' (&pound;', price, ')') as name, price"
        ." FROM model"
        ." WHERE brandId = ".$brandId
        ." ORDER BY price";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $array[] = $row;
    }

    echo json_encode($array);
    // Close DB connection
    $conn->close();
?>