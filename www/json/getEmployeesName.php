<?php
    include '../includes/db.php';
    $conn = openDbConnection();

    $array = array();
    $branchId = $_GET['branchId'];
    $sql = "SELECT id, CONCAT(firstName, ' ',lastName) AS name"
        ." FROM employee WHERE (roleId = 1 OR 2) AND branchID = ".$branchId;
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $array[] = $row;
    }

    echo json_encode($array);
    // Close DB connection
    $conn->close();
?>