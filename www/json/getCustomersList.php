<?php
    include '../includes/db.php';
    $conn = openDbConnection();

    $array = array();
    $sql = "SELECT customer.id, customer.firstName, customer.lastName, customer.tel,"
            ." `order`.id AS orderId FROM customer"
            ." LEFT JOIN `order` ON customer.id = `order`.customerId"
            ." ORDER BY id DESC";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $array[] = $row;
    }

    echo '{"data":'.json_encode($array).'}';
    // Close DB connection
    $conn->close();
?>