<?php
    include '../includes/db.php';
    $conn = openDbConnection();

    $array = array();
    $modelId = $_GET['modelId'];
    $sql = "SELECT modelEquipment.id, equipment.feature,"
        ." CONCAT(modelEquipment.details, ' (&pound;', modelEquipment.price, ')') as details,"
        ." modelEquipment.price"
        ." FROM modelEquipment"
        ." INNER JOIN equipment on modelEquipment.equipmentId = equipment.id"
        ." WHERE availability='Y' AND modelId = ".$modelId
        ." ORDER BY equipment.id, modelEquipment.price";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $array[] = $row;
    }

    echo json_encode($array);
    // Close DB connection
    $conn->close();
?>