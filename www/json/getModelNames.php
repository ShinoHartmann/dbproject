<?php
    include '../includes/db.php';
    $conn = openDbConnection();

    $array = array();
    $brandName = $_GET['brandName'];
    $sql = "SELECT model.`name` FROM model"
           ." INNER JOIN brand on model.brandId = brand.id"
           ." WHERE brand.`name` = '" .$brandName  ."'"
           ." ORDER BY price";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $array[] = $row;
    }

    echo json_encode($array);
    // Close DB connection
    $conn->close();
?>