<?php
    include '../includes/db.php';
    $conn = openDbConnection();

    $array = array();
    $sql = "SELECT employee.id, employee.firstName, employee.lastName, employee.username,"
            ." branch.branchName, role.role AS role FROM employee"
            ." INNER JOIN branch ON employee.branchId = branch.id"
            ." INNER JOIN role ON employee.roleId = role.id"
            ." ORDER BY id DESC";
    $result = $conn->query($sql);
    while($row = $result->fetch_assoc()) {
        $array[] = $row;
    }

    echo '{"data":'.json_encode($array).'}';
    // Close DB connection
    $conn->close();
?>