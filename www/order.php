<?php
    $orderId = null;
    if(isset($_POST['order'])){
        $orderId = $_POST['orderId'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order</title>
    <?php require 'includes/head.php'; ?>
</head>
<body>
<div class="container row">
    <div class="col-md-6" >
        <h2>Order</h2>
        <form method="post" action="db/saveOrder.php">
            <p id="id"><input type="number" class="form-control" placeholder="Order ID" name="id" required="required"/></P>
            <p><input type="date" class="form-control" placeholder="Order Date" value="<?php echo date('Y-m-d'); ?>"
                name="orderDate" required="required"/></p>
            <p><textarea id="memo" rows="4" class="form-control" placeholder="Memo" name="memo"></textarea></p>
            <div class="col-md-6"></div>
            <input type="submit" class="btn btn-primary col-md-6" name="setOrderDate" value="Order"/>
        </form>
        <a href="menu.php">Back to menu</a>
    </div>
</div>
<script>
    $(document).ready(function() {
        var orderId = <?php echo $orderId ?>;
        if(orderId != null && orderId != "") {
            console.log(orderId);
            $('#id')
                .empty()
                .append($('<input type="number" class="form-control" placeholder="OrderID" name="id" value="'
                + orderId + '" required="required"/>'));
        }
    });
</script>
</body>
</html>