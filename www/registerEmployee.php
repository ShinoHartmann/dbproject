<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register Employee</title>
    <?php
        require 'includes/head.php';
        include 'includes/db.php';
    ?>
</head>
<body>
<div class="container">
    <div class="col-md-6">
        <h2>Employee's Information</h2>
        <form id="customerForm" method="post" action="db/saveEmployeeInfo.php">
            <p><input id="fname" type="text" class="form-control" placeholder="First name" name="fname" required="required"/></P>
            <p><input id="lname" type="text" class="form-control" placeholder="Last name" name="lname" required="required"/></p>
            <p><textarea id="address" rows="4" class="form-control" placeholder="Address" name="address" required="required"></textarea></p>
            <p><input id="tel" type="tel" class="form-control" placeholder="Telephone number" name="tel" required="required"/></p>
            <p><input id="username" type="text" class="form-control" placeholder="Username" name="username" required="required"/></p>
            <p><input id="password" type="password" class="form-control" placeholder="Password" name="password" required="required"/></p>
            <p>
                <select id="branch" class="form-control" name="branch" required="required">
                    <?php
                        $conn = openDbConnection();
                        $sql = "SELECT id, branchName FROM branch";
                        $result = $conn->query($sql);
                        while($row = $result->fetch_assoc()) { ?>
                            <option value=<?php echo $row["id"] ?> ><?php echo $row["branchName"] ?></option>
                        <?php }
                            $conn->close();
                    ?>
                </select>
            </p>
            <p>
            <p><input id="startDate" type="date" class="form-control" name="startDate" value="<?php echo date('Y-m-d');?>" required="required"/></p>
                <select id="role" class="form-control" name="role" required="required">
                    <?php
                        $conn = openDbConnection();
                        $sql = "SELECT id, role FROM role";
                        $result = $conn->query($sql);
                        while($row = $result->fetch_assoc()) { ?>
                            <option value=<?php echo $row["id"] ?> ><?php echo $row["role"] ?></option>
                        <?php }
                            $conn->close();
                    ?>
                </select>
            </p>
            <div class="col-md-6"></div>
            <input type="submit" class="btn btn-primary col-md-6" name="submit"
                value="Save"/>
        </form>
        <a href="menu.php" >Back to menu</a>
    </div>
</div>
</body>
</html>