<?php
session_start();
// Check login status
if (!isset($_SESSION["AUTHORIZATION"])) {
  header("Location: logout.php");
  exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cardealer System Menu</title>
    <?php require 'includes/head.php' ?>
</head>
<body>
<div class="container">
    <div class="col-md-3">
        <p>Sales Menu</p>
        <ul>
            <li><a class="btn btn-primary col-md-12 menuBtm" href="configuration.php">Configure Car</a></li>
            <li><a class="btn btn-primary col-md-12 menuBtm" href="registerCustomer.php">Register Customer</a></li>
            <li><a class="btn btn-primary col-md-12 menuBtm" href="show/customers.php">Show Customers</a></li>
        </ul>
        <?php
        if($_SESSION["AUTHORIZATION"] != "Staff") {
            echo "<p>Admin menu</p>" ?>
        <ul>
            <?php
            echo '<li><a class="btn btn-primary col-md-12 menuBtm" href="registerEmployee.php">Register Employee</a></li>';
            echo '<li><a class="btn btn-primary col-md-12 menuBtm" href="show/employees.php">Show Employees</a></li>';
            echo '<li><a class="btn btn-primary col-md-12 menuBtm" href="addNewCar.php">Add Model</a></li>';
            } ?>
        </ul>
        <a href="logout.php">Logout</a>
    </div>
</div>
</body>
</html>
