<?php
session_start();
if (isset($_SESSION["AUTHORIZATION"])) {
  $errorMessage = "You are logged out";
}
else {
  $errorMessage = "Session has been timed out";
}
// clear variable
$_SESSION = array();
// delete cookie
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}
// clear session
@session_destroy();

?>

<!doctype html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>cardealer application logout</title>
  </head>
  <body>
  <div><?php echo $errorMessage; ?></div>
  <ul>
  <li><a href="login.php">login</a></li>
  </ul>
  </body>
</html>
