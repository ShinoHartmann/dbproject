<?php
    include '../includes/db.php';
    if(isset($_POST['configuration'])){
        // Create connection
        $conn = openDbConnection();
        try {
            $conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);
            // Store customer ID into order
            $sql ="INSERT INTO `order` (customerId, salesPerson)"
            ." VALUES (" . $_POST['customerId'] . ", " . $_POST['salesPerson'] . ");";
            if ($conn->query($sql) === FALSE) {
                throw new Exception("Error: " . $sql . "<br>" . $conn->error);
            }
            $customerId = $_POST["customerId"];
            // Get created orderID above
            $orderId;
            $sql = "SELECT id FROM `order`"
                ." WHERE customerId = " .$customerId
                ." ORDER BY id DESC"
                ." LIMIT 1";
            if ($conn->query($sql) === FALSE) {
                throw new Exception("Error: " . $sql . "<br>" . $conn->error);
            } else {
                $result = $conn->query($sql);
                while($row = $result->fetch_assoc()) {
                    $orderId = $row["id"];
                }
            }
            // Store car details into modelEquipment
            $optionList = $_POST['optionList'];
            if(!empty($optionList)) {
                $sql = "INSERT INTO orderEquipment (orderId, modelEquipmentId) VALUES";
                $len = count($optionList);
                foreach ($optionList as $index => $item) {
                    $sql .= "(" .$orderId .", " .$item .")";
                    if ($index != $len - 1) {
                        $sql .= ", ";
                    }
                }
                if ($conn->query($sql) === FALSE) {
                    throw new Exception("Error: " . $sql . "<br>" . $conn->error);
                } else {
                    header("Location: ../menu.php");
                }
            }
            $conn->commit();
            $conn->close();
        } catch(Exception $e) {
            $conn->close();
            exit($e);
        }
    }
?>
<a href="../menu.php" >Back to menu</a>