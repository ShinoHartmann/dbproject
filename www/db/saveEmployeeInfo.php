<?php
    include '../includes/db.php';
    if(isset($_POST['submit'])){
        $conn = openDbConnection();
        try {
            $sql = "INSERT INTO employee (firstName, lastName, address, tel, username, password, branchId, startDate, roleId)"
                    ." VALUES("
                    ."'".$_POST['fname']."', "
                    ."'".$_POST['lname']."', "
                    ."'".$_POST['address']."', "
                    ."'".$_POST['tel']."', "
                    ."'".$_POST['username']."', "
                    ."'".$_POST['password']."', "
                    .$_POST['branch'].", "
                    ."'".$_POST['startDate']."', "
                    .$_POST['role']
                    .");";
            if ($conn->query($sql) === FALSE) {
                throw new Exception("Error: " . $sql . "<br>" . $conn->error);
            } else {
                $conn->close();
                header("Location: ../menu.php");
            }
        } catch(Exception $e) {
            $conn->close();
            exit($e);
        }
    }
?>
<a href="../menu.php" >Back to menu</a>