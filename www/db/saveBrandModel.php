<?php
    include '../includes/db.php';
    if(isset($_POST['saveBrandModel'])){
        $conn = openDbConnection();
        try {
            $conn->begin_transaction(MYSQLI_TRANS_START_READ_WRITE);

            // Insert brand if not exists
            $sql = "INSERT IGNORE INTO brand (name) VALUES ('" .$_POST['brand'] ."')";
            $conn->query($sql);
            // Select the brand id
            $sql = "SELECT id FROM brand where name = '" .$_POST['brand'] ."'";
            $result = $conn->query($sql);
            $brandId = $result->fetch_row()[0];

            // Insert model if not exists
            $sql = "INSERT IGNORE INTO model (name, brandId) VALUES ('" .$_POST['model'] ."', " .$brandId .")";
            $conn->query($sql);
            // Select the model id
            $sql = "SELECT id FROM model where brandId = " .$brandId ." AND name ='" .$_POST['model'] ."'";
            $result = $conn->query($sql);
            $modelId = $result->fetch_row()[0];

            // Commit transaction and close the connection
            $conn->commit();
            $conn->close();
            header("Location: ../addNewEquipment.php?brandId="  .$brandId ."&modelId=" .$modelId);
        } catch(Exception $e) {
            $conn->close();
            exit($e);
        }
    }
?>
<a href="../menu.php">Back to menu</a>