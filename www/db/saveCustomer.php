<?php
    include '../includes/db.php';
    if(isset($_POST['submit'])){
        $conn = openDbConnection();
        try {
            $sql = "INSERT INTO customer (firstName, lastName, address, tel, email)"
                    ." VALUES("
                    ."'".$_POST['fname']."', "
                    ."'".$_POST['lname']."', "
                    ."'".$_POST['address']."', "
                    ."'".$_POST['tel']."', "
                    ."'".$_POST['email']."'"
                    .");";
            if ($conn->query($sql) === TRUE) {
                $conn->close();
                header("Location: ../menu.php");
            } else {
                throw new Exception("Error: " . $sql . "<br>" . $conn->error);
            }
        } catch(Exception $e) {
            $conn->close();
            exit($e);
        }
    }
?>
<a href="../menu.php" >Back to menu</a>