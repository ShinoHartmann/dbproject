<?php
    include 'includes/db.php';
    $brandId = $_GET['brandId'] ;
    $modelId = $_GET['modelId'] ;
    $conn = openDbConnection();
    $selfUrl = $_SERVER['PHP_SELF'] ."?brandId="  .$brandId ."&modelId=" .$modelId;
    // Update model price
    if(isset($_POST['updatePrice'])){
        $sql = "UPDATE model SET price = " .$_POST['modelPrice'] ." WHERE id=" .$modelId;
        if ($conn->query($sql) === TRUE) {
            $conn->close();
            header("Location: " .$selfUrl);
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    // Add new equipment
    if(isset($_POST['addNewEquipment'])){
        $sql = "INSERT INTO modelEquipment (modelId, equipmentId, details, price)"
        ." VALUES (" .$modelId .", " .$_POST['equipmentId'] .", '" .$_POST['details'] ."', " .$_POST['price'] .")";
        if ($conn->query($sql) === TRUE) {
            $conn->close();
            header("Location: " .$selfUrl);
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    // Update existing equipment
    if(isset($_POST['updateEquipment'])){
        $sql = "UPDATE modelequipment"
               ." SET details='" .$_POST['details']
               ."', price=" .$_POST['price']
               .", availability='" .$_POST['availability'] ."'"
               ." WHERE id=" .$_POST['id'];
        if ($conn->query($sql) === TRUE) {
            $conn->close();
            header("Location: " .$selfUrl);
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
    // Update existing equipment
    if(isset($_POST['deleteEquipment'])){
        $sql = "DELETE from modelequipment WHERE id=" .$_POST['id'];
        if ($conn->query($sql) === TRUE) {
            $conn->close();
            header("Location: " .$selfUrl);
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Equipment</title>
    <?php require 'includes/head.php';?>
</head>
<body>
<div class="container">
    <div class="row">
        <h2>Add/Change Model Price</h2>
        <?php
            /* Get brand name by id*/
            $sql = "SELECT `name` FROM brand WHERE id=".$brandId;
            $result = $conn->query($sql);
        ?>
        <div class="row">
            <p class="col-md-2 showLabel">Brand</p>
            <p class="col-md-10"><?php echo $result->fetch_row()[0] ?></p>
        </div>
        <?php
            /* Get model name by id*/
            $sql = "SELECT `name`, price FROM model WHERE id=".$modelId;
            $result = $conn->query($sql);
            while($row = $result->fetch_assoc()) { ?>
        <div class="row">
            <p class="col-md-2 showLabel">Model</p>
            <p class="col-md-10"><?php echo $row["name"]?></p>
        </div>
        <div class="row">
        <!-- Update model price -->
        <form method="POST" action="<?php echo $selfUrl ?>">
            <p class="col-md-2 showLabel" for="modelPrice">Model Price</p>
            <div class="col-md-2">
                <input id="modelPrice" type="number" step="0.01" class="form-control" name="modelPrice"
                placeholder="Model Price" value=<?php echo $row["price"] ?> />
            </div>
            <input type="submit" name="updatePrice" value="Update Model Price" class="btn btn-primary col-md-2"/>
        </form>
     </div>
    <?php } ?>
    <!-- Add new equipment -->
    <div class="row">
        <h2>Add Equipment</h2>
        <form method="POST" action="<?php echo $selfUrl ?>">
            <div class="col-md-2">
                <select id="equipmentType" class="col-md-1 form-control" name="equipmentId">
                    <?php
                        /* Get equipment type*/
                        $sql = "SELECT id, feature FROM equipment";
                        $result = $conn->query($sql);
                        while($row = $result->fetch_assoc()) { ?>
                            <option value=<?php echo $row["id"] ?>  data-price="0"> <?php echo $row["feature"] ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="col-md-5">
                <input type="text" class="form-control" name="details" placeholder="Equipment Details"/>
            </div>
            <div class="col-md-2">
                <input type="number" step="0.01" class="form-control" name="price" placeholder="Price"/>
            </div>
            <input type="submit" name="addNewEquipment" value="Add Equipment" class="btn btn-primary col-md-2"/>
        </form>
    </div>
    <!-- Modify existing equipment -->
        <h2>Modify Equipment</h2>
        <div class="row">
            <div class="col-md-1">
                <h4 class="text-right">Feature</h4>
            </div>
            <div class="col-md-5">
                <h4 class="text-center">Details</h4>
            </div>
            <div class="col-md-2">
                <h4 class="text-center">Price</div>
            <div class="col-md-2">
                <h4 class="text-center">Availability</h4>
            </div>
            <div class="col-md-2"></div>
        </div>
        <?php
        /* Get existing equipment */
        $sql = "SELECT modelEquipment.id, equipment.feature, details, price, availability FROM modelEquipment"
               ." INNER JOIN equipment on modelEquipment.equipmentId = equipment.id"
               ." WHERE modelId = " .$modelId;
        $result = $conn->query($sql);
        while($row = $result->fetch_assoc()) { ?>
        <div class="row">
            <form method="POST" action="<?php echo $selfUrl ?>">
                <input type="hidden" name="id" value=<?php echo $row["id"] ?>>
                <div class="col-md-1">
                    <p class="showLabel"><?php echo $row["feature"] ?></p>
                </div>
                <div class="col-md-5">
                    <?php $details = $row['details'] ?>
                    <input type="text" class="form-control" name="details" value='<?php echo $details ?>' />
                </div>
                <div class="col-md-2">
                    <input type="number" step="0.01" class="form-control text-right" name="price" placeholder="Price" value=<?php echo $row["price"] ?> />
                </div>
                <div class="col-md-2">
                    <select class="form-control" name="availability">
                        <?php $availability = $row["availability"] ?>
                        <option <?php if($availability == 'Y') echo "SELECTED"; ?> value="Y">Y</option>
                        <option <?php if($availability == 'N') echo "SELECTED"; ?> value="N">N</option>
                    </select>
                </div>
                <div class="col-md-1">
                    <input type="submit" name="updateEquipment" value="Update" class="btn btn-primary"/>
                </div>
                <div class="col-md-1">
                    <input type="submit" name="deleteEquipment" value="Delete" class="btn btn-primary"/>
                </div>
            </form>
        </div>
        <?php } ?>
    </div>

    <?php
    // Close DB connection
    $conn->close();
    ?>
    <div class="row">
        <a href="menu.php">Back to menu</a>
    </div>
</div>
</body>
</html>