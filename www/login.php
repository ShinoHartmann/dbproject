<?php
  session_start();
  // error message
  $errorMessage = "";

    // pressed login button
    if (isset($_POST["login"])) {
        include 'includes/db.php';
        $conn = openFirstDbConnection();
        $sql = "SELECT role.authorization from role"
            ." INNER JOIN employee on role.id = employee.roleId"
            ." WHERE username ='" .  $_POST["username"] ."'"
            ." AND `password` = password('" . $_POST["password"] ."')";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            // create new session ID
            session_regenerate_id(TRUE);
            $_SESSION["AUTHORIZATION"] = $result->fetch_row()[0];
            $conn->close();
            header("Location: menu.php");
            exit;
        } else {
            $errorMessage = "Incorrect username or password</br>";
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <?php require 'includes/head.php' ?>
    <style>
        .errorMsg {color: red;}
    </style>
</head>
<body>
<div class="container row">
    <div class="col-md-4">
        <h2>Login</h2>
        <form name="loginForm" action="<?php print($_SERVER['PHP_SELF']) ?>" method="POST">
            <p><input type="text" placeholder="Username" name="username" class="form-control" required/></P>
            <p><input type="password" placeholder="Password" name="password" class="form-control" required/></p>
            <p class="errorMsg"><?php echo $errorMessage ?></p>
            <input type="submit" name="login" class="btn btn-primary" value="Login"/>
        </form>
    </div>
</div>
</body>
</html>