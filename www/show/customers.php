<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>show</title>
    <?php require '../includes/head.php' ?>
</head>
<body>
<div class="container">
    <h2>Customer List</h2>
    <table id="customerList" class="table table-striped table-bordered row" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Tel</th>
            <th>Order ID</th>
        </tr>
        </thead>
    </table>
    <a href="../menu.php">Back to menu</a>
    <form method="post" class="row order" action="orderDetails.php">
        <div class="col-md-8"></div>
        <p class="col-md-2"><input type="text" class="form-control" placeholder="Order ID" name="orderId"/></P>
        <input type="submit" class="btn btn-primary col-md-2" name="orderDetails" value="Show"/>
    </form>
    <form method="post" class="row order" action="customerDetails.php">
        <div class="col-md-8"></div>
        <p class="col-md-2"><input type="text" class="form-control" placeholder="Customer ID" name="customerId"/></P>
        <input type="submit" class="btn btn-primary col-md-2" name="customerDetails" value="Show"/>
    </form>
</div>
<script>
    $(document).ready(function() {
        $('#customerList').DataTable({
            "ajax": "../json/getCustomersList.php",
            "columns": [
                { "data": "id" },
                { "data": "firstName" },
                { "data": "lastName" },
                { "data": "tel" },
                { "data": "orderId" }
            ]
        });
    });
</script>
</body>
</html>