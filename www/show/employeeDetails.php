<?php
    $employeeId = null;
    if(isset($_POST['employeeDetails'])){
        $employeeId = $_POST['employeeId'];
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employee Details</title>
    <?php
        require '../includes/head.php';
        include '../includes/db.php';
    ?>
</head>
<body>
<div class="container">
    <h2>Employee Details</h2>
    <?php
        $conn = openDbConnection();
        $sql = "SELECT employee.id, firstName, lastName, employee.address, employee.tel, username,"
                ." branch.branchName, startDate, role.role"
                ." FROM employee"
                ." INNER JOIN branch ON employee.branchId = branch.id"
                ." INNER JOIN role ON employee.roleId = role.id"
                ." WHERE employee.id =" .$employeeId;
        $result = $conn->query($sql);
        while($row = $result->fetch_assoc()) { ?>
            <div class="row">
                <p class="col-md-2 showLabel">ID</p>
                <p id="id" class="col-md-10"><?php echo $row["id"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">First Name</p>
                <p id="firstName" class="col-md-10"><?php echo $row["firstName"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Last Name</p>
                <p id="lastName" class="col-md-10"><?php echo $row["lastName"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Address</p>
                <p id="address" class="col-md-10"><?php echo $row["address"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Tel</p>
                <p id="tel" class="col-md-10"><?php echo $row["tel"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Username</p>
                <p id="username" class="col-md-10"><?php echo $row["username"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Branch</p>
                <p id="branchName" class="col-md-10"><?php echo $row["branchName"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Join date</p>
                <p id="startDate" class="col-md-10"><?php echo $row["startDate"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Role</p>
                <p id="role" class="col-md-10"><?php echo $row["role"] ?></p>
            </div>
    <?php }
    $conn->close();
    ?>
    <a class="row" href="employees.php">Back to employee list</a>
    <a class="row" href="../menu.php">Back to menu</a>
</div>
</body>
</html>