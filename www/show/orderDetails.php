<?php
    $orderId = null;
    if(isset($_POST['orderDetails'])){
        $orderId = $_POST['orderId'];
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Order Details</title>
    <?php
        require '../includes/head.php';
        include '../includes/db.php';
    ?>
</head>
<body>
<div class="container">
    <div class="row">
        <h2>Order Details</h2>
        <?php
            $conn = openDbConnection();
            /* Select order details by order ID*/
            try {
                $sql = "SELECT customerId, orderDate, vin, registrationNo, "
                       ." CONCAT(employee.firstName, ' ', employee.lastName) as salesPerson,"
                       ." totalPrice, memo"
                       ." FROM `order`"
                       ." INNER JOIN employee ON employee.id = `order`.salesPerson"
                       ." WHERE `order`.id=".$orderId;
                $result = $conn->query($sql);
                while($row = $result->fetch_assoc()) { ?>
                    <div class="row">
                        <p class="col-md-2 showLabel">Order ID</p>
                        <p class="col-md-10"><?php echo $orderId ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Customer ID</p>
                        <p class="col-md-10"><?php echo $row["customerId"] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Order Date</p>
                        <p class="col-md-10"><?php echo $row["orderDate"] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">VIN</p>
                        <p class="col-md-10"><?php echo $row["vin"] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Registration No</p>
                        <p class="col-md-10"><?php echo $row["registrationNo"] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Sales Person</p>
                        <p class="col-md-10"><?php echo $row["salesPerson"] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Total Price</p>
                        <p class="col-md-10"><?php echo $row["totalPrice"] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Memo</p>
                        <p class="col-md-10"><?php echo $row["memo"] ?></p>
                    </div>
            <?php }    ?>
        </div>
        <div class="row">
            <h2>Car Configuration Details</h2>
                <div class="row">
                    <?php
                    /* Select brand and model by order ID*/
                    $sql = "SELECT distinct brand.`name` as brand, model.`name` as model from brand"
                           ." INNER JOIN model on brand.id = model.brandId"
                           ." INNER JOIN modelEquipment me on me.modelId = model.id"
                           ." INNER JOIN orderEquipment oe on me.id = oe.modelEquipmentId"
                           ." where oe.orderId = ".$orderId;
                    $result = $conn->query($sql);
                    while($row = $result->fetch_assoc()) { ?>
                        <div class="row">
                            <p class="col-md-2 showLabel">Brand</p>
                            <p class="col-md-10"><?php echo $row["brand"] ?></p>
                        </div>
                        <div class="row">
                            <p class="col-md-2 showLabel">Model</p>
                            <p class="col-md-10"><?php echo $row["model"] ?></p>
                        </div>
                    <?php }
                    /* Select configuration details by order ID*/
                    $sql = "SELECT details FROM orderEquipment"
                           ." INNER JOIN modelEquipment ON orderEquipment.modelEquipmentId = modelEquipment.id"
                           ." WHERE orderId = " .$orderId
                           ." ORDER BY equipmentId asc";
                    $result = $conn->query($sql);
                    $equipment = array();
                    while($row = $result->fetch_assoc()) {
                        $equipment[] = $row['details'];
                    }?>
                    <div class="row">
                        <p class="col-md-2 showLabel">Order ID</p>
                        <p class="col-md-10"><?php echo $orderId ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Engine</p>
                        <p class="col-md-10"><?php echo $equipment[0] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Color</p>
                        <p class="col-md-10"><?php echo $equipment[1] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Seat</p>
                        <p class="col-md-10"><?php echo $equipment[2] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Wheel</p>
                        <p class="col-md-10"><?php echo $equipment[3] ?></p>
                    </div>
                    <div class="row">
                        <p class="col-md-2 showLabel">Options</p>
                        <p class="col-md-10">
                        <?php for($x = 4; $x < count($equipment); $x++) {
                            echo $equipment[$x] ."<br />";
                        }
                    $conn->close();
                    } catch(Exception $e) {
                        $conn->close();
                        exit($e);
                    }
                    ?></p>
                </div>
        </div>
        <div class="row">
            <form method="post" class="order" action="../order.php">
                <input type="hidden" class="form-control" placeholder="Order ID" name="orderId" value="<?php echo $orderId ?>"/></P>
                <input type="submit" class="btn btn-primary col-md-2" name="order" value="Order"/>
            </form>
            <div class="col-md-1"></div>
            <form method="post" class="order" action="../addVinRegNo.php">
                <input type="hidden" class="form-control" placeholder="Order ID" name="orderId" value="<?php echo $orderId ?>"/></P>
                <input type="submit" class="btn btn-primary col-md-2" name="addVinReg" value="Add VIN and Reg No"/>
            </form>
        </div>
    <a class="row" href="customers.php">Back to customer list</a>
    <a class="row" href="../menu.php">Back to menu</a>
</div>
</body>
</html>