<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employee List</title>
    <?php require '../includes/head.php' ?>
</head>
<body>
<div class="container">
    <h2>Employee List</h2>
    <table id="customerList" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>ID</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
            <th>Branch</th>
            <th>Role</th>
        </tr>
        </thead>
    </table>
    <a href="../menu.php">Back to menu</a>
    <form method="post" class="row order" action="employeeDetails.php">
        <div class="col-md-8"></div>
        <p class="col-md-2"><input type="text" class="form-control" placeholder="Employee ID" name="employeeId"/></P>
        <input type="submit" class="btn btn-primary col-md-2" name="employeeDetails" value="See Details"/>
    </form>
<script>
    $(document).ready(function() {
        $('#customerList').DataTable({
            "ajax": "../json/getEmployeesList.php",
            "columns": [
                { "data": "id" },
                { "data": "firstName" },
                { "data": "lastName" },
                { "data": "username" },
                { "data": "branchName" },
                { "data": "role" }
            ]
        });
    });
</script>
</body>
</html>