<?php
    $customerId = null;
    if(isset($_POST['customerDetails'])){
        $customerId = $_POST['customerId'];
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Customer Details</title>
    <?php
        require '../includes/head.php';
        include '../includes/db.php';
    ?>
</head>
<body>
<div class="container">
    <h2>Customers Details</h2>
    <?php
        $conn = openDbConnection();
        $sql = "SELECT * FROM customer WHERE id=".$customerId;
        $result = $conn->query($sql);
        while($row = $result->fetch_assoc()) { ?>
            <div class="row">
                <p class="col-md-2 showLabel">Customer ID</p>
                <p id="id" class="col-md-10"><?php echo $row["id"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">First Name</p>
                <p id="firstName" class="col-md-10"><?php echo $row["firstName"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Last Name</p>
                <p id="lastName" class="col-md-10"><?php echo $row["lastName"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Address</p>
                <p id="address" class="col-md-10"><?php echo $row["address"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Tel</p>
                <p id="tel" class="col-md-10"><?php echo $row["tel"] ?></p>
            </div>
            <div class="row">
                <p class="col-md-2 showLabel">Email Address</p>
                <p id="email" class="col-md-10"><?php echo $row["email"] ?></p>
            </div>
    <?php }
    $conn->close();
    ?>
    <a class="row" href="customers.php">Back to customer list</a>
    <a class="row" href="../menu.php">Back to menu</a>
</div>
</body>
</html>