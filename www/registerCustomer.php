<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Register Customer</title>
    <?php
        require 'includes/head.php';
        include 'includes/db.php';
    ?>
</head>
<body>
<div class="container row">
    <div class="col-md-6" >
        <h2>Customer Information</h2>
        <form id="customerForm" method="post" action="db/saveCustomer.php">
            <p><input id="fname" type="text" class="form-control" placeholder="First name" name="fname" required="required"/></P>
            <p><input id="lname" type="text" class="form-control" placeholder="Last name" name="lname" required="required"/></p>
            <p><textarea id="address" rows="4" class="form-control" placeholder="Address" name="address" required="required"></textarea></p>
            <p><input id="tel" type="tel" class="form-control" placeholder="Telephone number" name="tel" required="required"/></p>
            <p><input id="email" type="email" class="form-control" placeholder="Email address" name="email" required="required"/></p>
            <div class="col-md-6"></div>
            <input type="submit" class="btn btn-primary col-md-6" name="submit" value="Save"/>
        </form>
        <a href="menu.php" >Back to menu</a>
    </div>
</div>
</body>
</html>