<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add Car</title>
    <?php
        require 'includes/head.php';
        include 'includes/db.php';
    ?>
    <script type="text/javascript" src="javaScript/addNewCar.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <form id="selection" method="post" class="col-md-6"action="db/saveBrandModel.php">

            <p>
            <label>Brand:</label>
            <input id="brand" list="brandList" name="brand" class="form-control"?required="required"></input>
                <datalist id="brandList">
                <?php
                $conn = openDbConnection();
            $sql = "SELECT name FROM brand ORDER BY id";
            $result = $conn->query($sql);
            while($row = $result->fetch_assoc()) { ?>
                <option><?php echo $row["name"] ?></option>
            <?php }
             // Close DB connection
              $conn->close();?>
            </p>
            <p>
                <label>Model:</label>
                <input id="model" list="modelList" name="model" class="form-control"?required="required"></input>
                <datalist id="modelList"></datalist>
            </p>
            <input type="submit" class="btn btn-primary col-md-5" name="saveBrandModel" value="Add Equipment"/>
        </form>
    </div>
    <div class="row">
        <a class="col-md-3" href="menu.php">Back to menu</a>
    </div>
</div>
</body>
</html>