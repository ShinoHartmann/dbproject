<?php
    $orderId = null;
    if(isset($_POST['addVinReg'])){
        $orderId = $_POST['orderId'];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Add VIN Reg No</title>
    <?php require 'includes/head.php'; ?>
</head>
<body>
<div class="container row">
    <div class="col-md-6" >
        <h2>Add VIN Reg No</h2>
        <form method="post" action="db/saveVinRegNo.php">
            <p id="id"><input type="number" class="form-control" placeholder="Order ID" name="id" required="required"/></P>
            <p><input type="text" class="form-control" placeholder="VIN" name="vin" required="required"/></P>
            <p><input type="text" class="form-control" placeholder="Registration No." name="registrationNo" required="required"/></P>
            <div class="col-md-6"></div>
            <input type="submit" class="btn btn-primary col-md-6" name="saveVinRegNo" value="Add"/>
        </form>
        <a href="menu.php">Back to menu</a>
    </div>
</div>
<script>
    $(document).ready(function() {
        var orderId = <?php echo $orderId ?>;
        if(orderId != null && orderId != "") {
            console.log(orderId);
            $('#id')
                .empty()
                .append($('<input type="number" class="form-control" placeholder="Order?ID" name="id" value="'
                + orderId + '" required="required"/>'));
        }
    });
</script>
</body>
</html>