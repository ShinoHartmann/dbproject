<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Choose model</title>
    <?php
        require 'includes/head.php';
        include 'includes/db.php';
    ?>
    <script type="text/javascript" src="javaScript/configuration.js"></script>
</head>
<body>
<div class="container row">
    <h2>Car Configuration</h2>
        <form id="selection" method="post" action="db/saveConfiguration.php">
            <div class="col-md-6">
                <p>
                    <label>Branch:</label>
                    <select id="branch" class="form-control" name="branch" required="required">
                    <option selected="true" data-price="0" disabled="disabled">Please select Branch name</option>
                        <?php
                            $conn = openDbConnection();
                            $sql = "SELECT id, branchName FROM branch";
                            $result = $conn->query($sql);
                            while($row = $result->fetch_assoc()) { ?>
                                <option value=<?php echo $row["id"]?>  data-price="0" ><?php echo $row["branchName"] ?></option>
                            <?php }
                                $conn->close();
                        ?>
                    </select>
                </p>
                <p>
                    <label>Sales Person:</label>
                    <select id="salesPerson" class="form-control" name="salesPerson" required="required">
                    </select>
                </p>
                <p>
                    <label>Brand:</label>
                    <select id="brand" name="brand" class="form-control" required>
                        <option selected="true" disabled="disabled">Please select a brand</option>
                        <?php
                            $conn = openDbConnection();
                            $sql = "SELECT id, name FROM brand ORDER BY name";
                            $result = $conn->query($sql);
                            while($row = $result->fetch_assoc()) { ?>
                                <option value=<?php echo $row["id"] ?>  data-price="0"> <?php echo $row["name"] ?></option>
                            <?php }
                                $conn->close();
                        ?>
                    </select>
                </p>
                <p>
                    <label>Customer ID:</label>
                    <input type="text" id="customerId" name="customerId" 'data-price': "0" placeholder="Customer ID" class="form-control" required/>
                </p>

                <p>
                    <label>Model:</label>
                    <select id="model" name="model" class="form-control" required></select>
                </p>
                <p>
                    <label>Engine (required):</label>
                    <select id="engine" name="optionList[]" class="form-control" required></select>
                </p>
                <p>
                    <label>Color (required):</label>
                    <select id="color" name="optionList[]" class="form-control" required></select>
                </p>
                <p>
                    <label>Seat (required):</label>
                    <select id="seat" name="optionList[]" class="form-control" required></select>
                </p>
                <p>
                    <label>Wheel (required):</label>
                    <select id="wheel" name="optionList[]" class="form-control" required></select>
                </p>
            </div>
            <div class="col-md-12">
                <h3>Options</h3>
                <div id="option"></div>
                <h3 id="totalPrice"></h3>
                <br />
                <input type="submit" name="configuration" value="Save" class="btn btn-primary col-md-6"/>
            </div>
        </form>
        <a class="col-md-12" href="menu.php">Back to menu</a>
    </div>
</div>
</body>
</html>