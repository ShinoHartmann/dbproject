$(document).ready(function() {
    $("#brand").change(changeModelName);
});

function changeModelName() {
    $('#modelList').empty();
    $('#model').val('');
    var brandName = $("#brand").val();
    $.getJSON("json/getModelNames.php?brandName=" + brandName, function(json){
        $.each(json, function(key, value) {
             $('#modelList')
                 .append($("<option/>")
                 .html(value.name));
        });
    });
}
