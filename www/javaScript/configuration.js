$(document).ready(function() {
    clearAllSelections();
    $("#branch").change(changeEmployeeName);
    $("#brand").change(changeModelName);
    $("#model").change(changeEquipment);
    $("#engine").change(showTotalPrice);
    $("#color").change(showTotalPrice);
    $("#seat").change(showTotalPrice);
    $("#option").change(showTotalPrice);
});

function changeEmployeeName() {
    $('#salesPerson')
        .empty()
        .append('<option selected="true"  data-price="0" disabled="disabled">Sales Person</option>');
    $.getJSON("json/getEmployeesName.php?branchId=" + $("#branch").val(), function(json){
        $.each(json, function(key, value) {
             $('#salesPerson')
                 .append($("<option></option>")
                 .attr({
                     value: value.id,
                     "data-price": "0"
                 })
                 .text(value.name));
        });
    });
}

function changeModelName() {
    clearAllSelections();
    var brandId = $("#brand").val();
    $.getJSON("json/getModels.php?brandId=" + brandId, function(json){
        $.each(json, function(key, value) {
             $('#model')
                 .append($("<option/>")
                 .attr({
                        value: value.id,
                        "data-price": value.price
                    })
                 .html(value.name));
        });
    });
}

function clearAllSelections() {
    $('#model')
        .empty()
        .append('<option selected="true"  data-price="0" disabled="disabled">Model</option>');
    clearEquipmentSelections();
    showTotalPrice();
    }

function clearEquipmentSelections() {
    $('#engine')
        .empty();
    $('#color')
        .empty();
    $('#seat')
        .empty();
    $('#wheel')
        .empty();
    $('#option')
        .empty();
    showTotalPrice();
}

function changeEquipment() {
    clearEquipmentSelections();
    var modelId = $('#model').val();
    $.getJSON("json/getEquipment.php?modelId="+modelId, function(json){
        $.each(json, function(key, value) {
            if(value.feature == "Option") {
                $("#option")
                    .append($("<input/>")
                        .attr({
                            id: value.id,
                            type: "checkbox",
                            value: value.id,
                            name: "optionList[]",
                            'data-price': value.price
                        }))
                     .append($("<label/>").attr('for', value.id).html(value.details))
                     .append($("<br/>"));
            } else {
                $("#"+value.feature.toLowerCase())
                    .append($("<option/>")
                        .attr({
                            value: value.id,
                            'data-price': value.price
                        })
                        .html(value.details)
                    );
            }
        });
    });
}

function calculatePrice() {
    var sum = 0;
    console.log(sum);
    $('#selection').each(function(){
        $(this).find('option:selected').each(function(i,n){
            sum += parseFloat($(n).data('price'));
        });
    });
    console.log(sum);
    $('#option').each(function(){
        $(this).find('input:checked').each(function(i,n){
            sum += parseFloat($(n).data('price'));
        });
    });
    console.log(sum);
    return sum.toFixed(2);
}

function showTotalPrice() {
    var sum = calculatePrice();
    $('#totalPrice').empty();
    $("#totalPrice").text("Total Price: \u00A3" + sum);
}